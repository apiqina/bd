/*******TRIGGERS*********/
/*Trigger lors d'un insert/delete/update d'un etudiant*/
INSERT INTO Statistique VALUES (0,0,0,0,0,0,0);

CREATE OR REPLACE TRIGGER BEFORE_IDU_ETUDIANT BEFORE INSERT OR UPDATE OR DELETE
	ON Etudiant
	FOR EACH ROW
BEGIN

	/*Les etudiants doivent etre de l'annee courrante*/
  IF(:NEW.finAnnee = TO_DATE('2016','yyyy')) THEN
			UPDATE Statistique s
			SET s.Stagiaire = stagiaireA;
          
      UPDATE Statistique s
			SET s.nonStagiaire = stagiaireS;

	END IF;
END;