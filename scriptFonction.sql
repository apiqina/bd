/****** Fonction *************/
/*****r�cup�rer le nombre d'�tudiants avec stage cette ann�e******/
create or replace FUNCTION stagiaireA RETURN INTEGER IS nbEtuStagiaire INTEGER;
BEGIN 
    SELECT COUNT (*) INTO nbEtuStagiaire
    FROM Etudiant e
    WHERE e.stagiaire = 'oui' 		
    AND TO_CHAR(e.finAnnee, 'YYYY')=TO_CHAR(sysdate, 'YYYY');	
RETURN nbEtuStagiaire;
END stagiaireA;

/****r�cup�rer le nombre d'�tudiants sans stage cette ann�e*****/
create or replace Function stagiaireS RETURN INTEGER IS nbEtuSansS INTEGER;
BEGIN
    SELECT COUNT(*) INTO nbEtuSansS
    FROM Etudiant e
    WHERE e.stagiaire = 'non'
    AND TO_CHAR(e.finAnnee, 'YYYY')= TO_CHAR(sysdate, 'YYYY');
RETURN nbEtuSansS;
END stagiaireS;
    
/******r�cup�rer le nombre d'�tudiants sans stage � une certaine date pour une ann�e choisie par l'utilisateur******/    
create or replace Function stagiaireAnnee(annee INT) RETURN INTEGER IS nbEtuSansAnnee INTEGER;
BEGIN
    SELECT COUNT(*) INTO nbEtuSansAnnee
    FROM Etudiant e
    WHERE e.stagiaire = 'non'
    AND TO_CHAR(e.finannee, 'YYYY')= TO_CHAR(annee, 'YYYY');
RETURN nbEtuSansAnnee;
END stagiaireAnnee;

/*******le nombre de stages pour toutes les zones g�ographiques*******/
create or replace FUNCTION nbStageZones RETURN INTEGER IS nbStage INTEGER;
BEGIN 
  SELECT COUNT(*) INTO nbStage 
  FROM Stage s
  GROUP BY DEREF(s.entreprise).adresse.departement;
RETURN nbStage;
END  nbStageZones;

/*******le nombre de stages par zone g�ographique choisi par l'utilisateur (departement, ville)*******/
create or replace FUNCTION nbStageParZone(dep VARCHAR2, city VARCHAR2 ) RETURN INTEGER IS nbStage INTEGER;
BEGIN 
  SELECT Count(*) INTO nbStage
  FROM Stage S
  WHERE  DEREF(S.Entreprise).adresse.departement = dep 
  AND  DEREF(S.Entreprise).adresse.ville = city;
RETURN nbStage;
END  nbStageParZone;

/********le nombre de stagiaires pris par chaque entreprise durant les n derni�res ann�es****/
create or replace Function stagiaireEnt(annee INT) RETURN INTEGER IS nbEtu INTEGER;
BEGIN
    SELECT COUNT(*) INTO nbEtu, DEREF(s.entreprise) int ent, 
    FROM Etudiant etu, Entreprise e, Stage s
    WHERE etu.stagiaire = 'oui'
    AND DEREF(s.etudiant)id = etu.id
    AND etu.finAnnee 
    GROUP BY ent.id
RETURN nbEtu ;
END stagiaireEnt;