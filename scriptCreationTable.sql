/***** Type Adresse ******/
CREATE OR REPLACE TYPE ObAdresseTy AS OBJECT (
numRue      INT,
nomRue	    VARCHAR2 (50),
ville       VARCHAR2 (50),
departement VARCHAR2 (50),
Pays        VARCHAR2 (50)
);

/***** Type Entreprise ******/
CREATE OR REPLACE TYPE ObEntrepriseTy AS OBJECT (
num       INT,	
nom       VARCHAR2 (50),
adresse   ObAdresseTy,
numTel 	INT  
);

/***** Type Etudiant ******/
CREATE OR REPLACE TYPE obEtudiantTy AS OBJECT(
num       INT,  	
nom       VARCHAR2 (50),    
prenom    VARCHAR2 (50),   
email     VARCHAR2 (50),   
debutAnnee DATE,	  
finAnnee   DATE,      
stagiaire VARCHAR(20)	/*determine si l'etudiant a un stage ou non*/
);

/***** Type Stage ******/
CREATE OR REPLACE  TYPE ObStageTy  AS OBJECT(    
num          INT,
nom          VARCHAR2 (50),
dateDebut    DATE,
dateFin		   DATE,
entreprise  REF ObEntrepriseTy,
etudiant    REF ObEtudiantty
);

/***** Type Statistique ******/
CREATE OR REPLACE TYPE ObStatistiqueTy AS OBJECT (
stagiaire              INT,	    	 /* nb �tudiants avec stage cette ann�e*/
nonStagiaire           INT, 	     /* nb �tudiants sans stage cette ann�e*/
nonStagiareAnneeN      INT,         /* nb �tudiants sans stage � une ann�e N (donn�e) utlt�rieure*/
stagiaireParEntreprise INT,         /* nb  stagiaires pris par chaque entreprise durant les n derni�res ann�es*/
moyStagiaire           FLOAT, 	     /*nb moyen de stagiaires encadr�s par les entreprises dans les n derni�res ann�es*/
stageParZone           INT,          /*nb stages par zone g�ographique choisi par l'utilisateur (d�partement, ville)*/
stageTot               INT    /* nb stages pour toutes les zones g�ographiques (d�partement, ville)*/
);


/*cr�ation des tables : on ne cr�er pas la table ADRESSE car elle n'est d'aucune utilit� vu qu'on a d�j� l'objet adresse*/
CREATE TABLE ENTREPRISE of ObEntrepriseTy (num  PRIMARY KEY);
CREATE TABLE STAGE of ObStageTy (num  PRIMARY KEY);
CREATE TABLE ETUDIANT of ObEtudiantTy (num  PRIMARY KEY,CHECK (debutAnnee < finAnnee ) );
CREATE TABLE STATISTIQUE of ObStatistiqueTy;
