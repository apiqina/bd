/*INSERT POUR ENTREPRISE*/
INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(01,'Total',ObAdresseTy(07,'rue la vallet','Paris','75','France'),0102030405));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(02,'Carrefour',ObAdresseTy(69,'rue des Ulis ','Les Ulis','91','France'),0169295979));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(03,'Credit Agricole',ObAdresseTy(2,'rue Verrier ','Orsay','91','France'),0164861750));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(04,'GDF Suez',ObAdresseTy(49,'rue de la Paix ','Paix','75','France'),0145657820));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(05,'Bouygues',ObAdresseTy(12,'rue de Vaugirard ','Cachan','94','France'),0189265245));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(06,'Carrefour Contact',ObAdresseTy(3,'avenue de lOc�an','La Palmyre','17','France'),0546236556));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(07,'Alibaba',ObAdresseTy(969,'West Wen Yi Road','Hangzou','31','Chine'),7185022088));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(08,'Bank of India',ObAdresseTy(5,'Bandra Kurla Complex','Mumbai','40','Inde'),2266684444));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(09,'Jovenna',ObAdresseTy(10,'Zone Galaxy Andraharo','Antananarivo','101','Madagascar'),1202369470));

INSERT INTO Entreprise 
VALUES (ObEntrepriseTy(10,'CD PROJEKT S.A.',ObAdresseTy(74,'Jagiellonska','Warszawa','03','Pologne'),8225196900));


/*INSERT POUR ETUDIANT*/
INSERT INTO Etudiant
VALUES (01,'Radja','Anita','anita.radja@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (02,'Rivoherinjakanavalona','Nathalie','nathalie.rivoherinjakanavalona@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (03,'Xu','Qin','qin.xu@u-psud.fr',TO_DATE('2014', 'yyyy'),TO_DATE('2015', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (04,'Straebler','Pierre','pierre.straebler@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (05,'Warchol','Patryk','patryk.warchol@u-psud.fr',TO_DATE('2014', 'yyyy'),TO_DATE('2015', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (06,'Ha','David','david.ha@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (07,'Allermoz','Florian','florian.allermoz@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (08,'Aparicio','Benjamin','benjamin.aparicio@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (09,'Ould Ouali','Lydia','lydia.ould-ouali@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (10,'Kui','Stephane','st�ephane.kui@u-psud.fr',TO_DATE('2014', 'yyyy'),TO_DATE('2015', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (11,'Etienne','Angeelique','angelique.etienne@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (12,'Cherpentier','Antoine','antoine.cherpentier@u-psud.fr',TO_DATE('2013', 'yyyy'),TO_DATE('2014', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (13,'Sauvard','Clement','clement.sauvard@u-psud.fr',TO_DATE('2014', 'yyyy'),TO_DATE('2015', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (14,'Gras','Johan','johan.gras@u-psud.fr',TO_DATE('2013', 'yyyy'),TO_DATE('2014', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (15,'Detchberry','Valentin','valentin.detchberry@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'non');

INSERT INTO Etudiant
VALUES (16,'Ragon','Cl�mence','clemence.ragon@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (17,'Ecollan','Charles','charles.ecollan@u-psud.fr',TO_DATE('2014', 'yyyy'),TO_DATE('2015', 'yyyy'),'oui');

INSERT INTO Etudiant
VALUES (18,'Bresson','Quentin','quentin.bresson@u-psud.fr',TO_DATE('2015', 'yyyy'),TO_DATE('2016', 'yyyy'),'oui');


/*INSERT POUR STAGE*/
INSERT INTO Stage
SELECT 01,'Administrateur Syst�me et R�seaux',TO_DATE('11/04/2014', 'dd/mm/yyyy'),TO_DATE('01/07/2014', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 01
AND et.num = 06;

INSERT INTO Stage
SELECT 02,'Chercheur en informatique',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 02
AND et.num = 07;

INSERT INTO Stage
SELECT 03,'Responsable S�curit� Informatique',TO_DATE('11/04/2014', 'dd/mm/yyyy'),TO_DATE('01/07/2014', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 03
AND et.num = 08;

INSERT INTO Stage
SELECT 04,'Superviseur de hot-line',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 04
AND et.num = 09;

INSERT INTO Stage
SELECT 05,'Webmaster',TO_DATE('11/04/2015', 'dd/mm/yyyy'),TO_DATE('01/07/2015', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 05
AND et.num = 10;

INSERT INTO Stage
SELECT 13,'Responsable des r�seaux de t�l�communication',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 05
AND et.num = 11;

INSERT INTO Stage
SELECT 06,'Int�grateur Web',TO_DATE('11/04/2014', 'dd/mm/yyyy'),TO_DATE('01/07/2014', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 06
AND et.num = 12;

INSERT INTO Stage
SELECT 07,'Ing�nieur d�velopement logiciel',TO_DATE('11/04/2015', 'dd/mm/yyyy'),TO_DATE('01/07/2015', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 07
AND et.num = 13;

INSERT INTO Stage
SELECT 08,'D�velopeur d�cisionnel',TO_DATE('11/04/2014', 'dd/mm/yyyy'),TO_DATE('01/07/2014', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 07
AND et.num = 14;

INSERT INTO Stage
SELECT 09,'Chef de projet',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 08
AND et.num = 15;

INSERT INTO Stage
SELECT 10,'Expert securite informatique',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 09
AND et.num = 16;

INSERT INTO Stage
SELECT 11,'D�velopeur jeux vid�o',TO_DATE('11/04/2015', 'dd/mm/yyyy'),TO_DATE('01/07/2015', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 10
AND et.num = 17;

INSERT INTO Stage
SELECT 12,'Charg� de r�f�rencement',TO_DATE('11/04/2016', 'dd/mm/yyyy'),TO_DATE('01/07/2016', 'dd/mm/yyyy'), REF(e), REF(et)
FROM Entreprise e, Etudiant et
WHERE e.num = 04
AND et.num = 18;

