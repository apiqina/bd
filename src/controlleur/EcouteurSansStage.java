package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vue.VueMenu;
import vue.VueSansStage;

public class EcouteurSansStage implements ActionListener {


	private VueSansStage vueSansStage;
	private VueMenu vueMenu;

	public EcouteurSansStage(VueSansStage vue){
		this.vueSansStage = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		JButton b = (JButton) e.getSource();

		if (b.getText() == "Retour"){

			this.vueSansStage.dispose();
			this.vueMenu = new VueMenu();
		}
	}

}