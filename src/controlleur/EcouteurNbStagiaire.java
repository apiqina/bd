package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vue.VueAvecStage;
import vue.VueMenu;
import vue.VueNbStagiaire;

public class EcouteurNbStagiaire implements ActionListener {

	private VueNbStagiaire vueNbStagiaire;
	private VueMenu vueMenu;
	
	public EcouteurNbStagiaire(VueNbStagiaire vue){
		this.vueNbStagiaire = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
JButton b = (JButton) e.getSource();
		
		if (b.getText() == "Retour"){
			
			this.vueNbStagiaire.dispose();
			this.vueMenu = new VueMenu();
		}
	}

}