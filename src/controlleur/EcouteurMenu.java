package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vue.VueAvecStage;
import vue.VueMenu;
import vue.VueNbStagiaire;
import vue.VueSansStage;

public class EcouteurMenu implements ActionListener {
	
	private VueMenu vue;
	private VueAvecStage vueAvecStage;
	private VueSansStage vueSansStage;
	private VueNbStagiaire vueNbStagiaire;
	public EcouteurMenu(VueMenu vue){
		this.vue = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		JButton b = (JButton) e.getSource();
		
		if (b.getText() == "  Nombre d'�tudiants avec stage  "){
			
			this.vue.dispose();
			this.vueAvecStage = new VueAvecStage(2);
		}
		
		else if (b.getText() == "  Nombre d'�tudiants sans stage  "){
			this.vue.dispose();
			this.vueSansStage = new VueSansStage(4);
		}
		
		else if (b.getText() == "  Nombre de stagiaires par entreprise pour n derni�res ann�es  "){
			this.vue.dispose();
			this.vueNbStagiaire = new VueNbStagiaire(5);
		}
		
		else if (b.getText() == "  Nombre moyen de stagiaires encadr�s par les entreprises pour n derni�res ann�es  "){
			this.vue.dispose();
			
		}
	}

}