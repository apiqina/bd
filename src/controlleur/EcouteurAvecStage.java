package controlleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import vue.VueAvecStage;
import vue.VueMenu;

public class EcouteurAvecStage implements ActionListener {
	
	
	private VueAvecStage vueAvecStage;
	private VueMenu vueMenu;
	
	public EcouteurAvecStage(VueAvecStage vue){
		this.vueAvecStage = vue;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
JButton b = (JButton) e.getSource();
		
		if (b.getText() == "Retour"){
			
			this.vueAvecStage.dispose();
			this.vueMenu = new VueMenu();
		}
	}

}