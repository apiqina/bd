/**
 * PROJET BD : TPA2
 * RIVOHERINJAKANAVALONA Nathalie
 * RADJA Anita
 * QIN XU
 * STRAEBLER Pierre 
 * 
 * classe Donnee qui va recuperer les donn�es de la table Statistique
 * 				
 */
package classe;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Donnee {
	/**
	 * Attributs
	 */
	int nbEt1;
	int nbEt2; 
	int nbEt3;  
	int nbEt4; 
	int moy;  
	int nbStageZone; 
	int nbStage; 

	/**
	 * Constructeur de la classe
	 */
	public Donnee(){

		String url="jdbc:oracle:thin:aradja/rachanke1996@oracle.iut-orsay.fr:1521:etudom";
		Connection co = JDBC.openConnection(url);
		try{
			/*****r�cup�rer le nombre d'�tudiants avec stage cette ann�e******/
			String req = "SELECT * FROM STATISTIQUE S";
			ResultSet rs = JDBC.exec1Requete(req, co, 1);
			while (rs.next()){
				this.nbEt1 = rs.getInt(1);
				this.nbEt2 = rs.getInt(2);
				this.nbEt2 = rs.getInt(2);
				this.nbEt3 = rs.getInt(3);
				this.nbEt4 = rs.getInt(4);
				this.moy = rs.getInt(5) ;
				this.nbStageZone = rs.getInt(6);
				this.nbStage = rs.getInt(7);
			}	


		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		//traitement	et requete OutilsJDBC.closeConnection(co);
		System.out.println();
		JDBC.closeConnection(co);


	}

	/**
	 * fonction qui retourne 1er attribut de Main
	 * @return int
	 */
	public int getNbEt1() {
		return nbEt1;
	}


	/**
	 * fonction qui retourne 2eme attribut de Main
	 * @return int
	 */
	public int getNbEt2() {
		return nbEt2;
	}

	/**
	 * fonction qui retourne 3eme attribut de Main
	 * @return int
	 */
	public int getNbEt3() {
		return nbEt3;
	}

	/**
	 * fonction qui retourne 4eme attribut de Main
	 * @return int
	 */
	public int getNbEt4() {
		return nbEt4;
	}
	
	/**
	 * fonction qui retourne 5eme attribut de Main
	 * @return int
	 */
	public int getMoy() {
		return moy;
	}

	/**
	 * fonction qui retourne 6eme attribut de Main
	 * @return int
	 */
	public int getNbStageZone() {
		return nbStageZone;
	}

	/**
	 * fonction qui retourne 7eme attribut de Main
	 * @return int
	 */
	public int getNbStage() {
		return nbStage;
	}
}
