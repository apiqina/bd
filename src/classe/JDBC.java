/**
 * PROJET BD : TPA2
 * RIVOHERINJAKANAVALONA Nathalie
 * RADJA Anita
 * QIN XU
 * STRAEBLER Pierre 
 * 
 * classe JDBC - Classe qui contient les fonctions qui assurent l'ouverture/fermeture 
 * de la connexion ainsi que la fonction qui execute les Requetes
 */
package classe;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBC {

	/**
	 * Fonction qui ouvre la connexion
	 * @param url
	 * @return Connection
	 */
	public static Connection openConnection (String url) {
		Connection co=null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			co= DriverManager.getConnection(url);
			System.out.println("Connexion ouverte!");
		}
		catch (ClassNotFoundException e){
			System.out.println("il manque le driver oracle");
			System.exit(1);
		}
		catch (SQLException e) {
			System.out.println("impossible de se connecter � l'url : "+url);
			System.exit(1);
		}
		return co;
	}
	
	/**
	 * Fonction qui permet l'execution des requetes
	 * @param requete
	 * @param co
	 * @param type
	 * @return ResultSet
	 */
	public static ResultSet exec1Requete (String requete, Connection co, int type){
		ResultSet res=null;
		try {
			Statement st;
			if (type==0){
				st=co.createStatement();}
			else {
				st=co.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
						ResultSet.CONCUR_READ_ONLY);
			};
			res= st.executeQuery(requete);
		}
		catch (SQLException e){
			System.out.println("Probl�me lors de l'ex�cution de la requete : "+requete);
		};
		return res;
	}

	/**
	 * Fonction pour fermer la connexion
	 * @param co
	 */
	public static void closeConnection(Connection co){
		try {
			co.close();
			System.out.println("Connexion ferm�e!");
		}
		catch (SQLException e) {
			System.out.println("Impossible de fermer la connexion");
		}
	}
	
}
