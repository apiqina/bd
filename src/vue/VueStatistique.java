/**
 * PROJET BD : TPA2
 * RIVOHERINJAKANAVALONA Nathalie
 * RADJA Anita
 * QIN XU
 * STRAEBLER Pierre 
 * 
 * classe Vue Statique - Vue qui affiche les �l�ments de la Table Statique 
 */
package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.Main;
import classe.Donnee;


/**
 * 
 *Frame contenant table statique
 *
 */
public class VueStatistique extends JFrame{
	
	
	private Donnee donnee;
	
	private JPanel pnlTab;
	private JLabel titre;
	
	private JLabel att1;
	private JLabel att2;
	private JLabel att3;
	private JLabel att4;
	private JLabel att5;
	private JLabel att6;
	private JLabel att7;
	
	/**
	 * Constructeur de la classe
	 */
	public VueStatistique(){
		
		this.setTitle("Statistiques");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		this.setSize(new Dimension(1000, 500));
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		

	
		this.donnee = new Donnee();
		this.pnlTab = new JPanel();
		this.pnlTab.setLayout(new GridLayout(8, 1));

		//Titre
		this.titre = new JLabel(" STATISTIQUES");
		this.pnlTab.add(titre);
		Font fontEnteredTitre = new Font(Font.DIALOG, Font.BOLD, 30);
		this.titre.setBackground(new Color(207,227,240));
		this.titre.setOpaque(true);
		this.titre.setFont(fontEnteredTitre);
		this.titre.setForeground(new Color(0,51,102));
		this.titre.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.titre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);;
		
		
		
		Font fontEntered = new Font(Font.DIALOG, Font.CENTER_BASELINE, 20);
		
		//ligne 1
		this.att1 = new JLabel("\t"+"\t"+"Nomdre d'�tudiant avec un stage : "+"\t"+"\t"+ donnee.getNbEt1());
		this.pnlTab.add(att1);
		
		this.att1.setBackground(new Color(207,227,240));
		this.att1.setOpaque(true);
		this.att1.setFont(fontEntered);
		this.att1.setForeground(new Color(0,51,102));
		this.att1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		
		//ligne 2
		this.att2 = new JLabel("\t"+"\t"+"Nomdre d'�tudiant sans stage : "+"\t"+"\t"+ donnee.getNbEt2());
		this.pnlTab.add(att2);

		this.att2.setBackground(new Color(207,227,240));
		this.att2.setOpaque(true);
		this.att2.setFont(fontEntered);
		this.att2.setForeground(new Color(0,51,102));
		this.att2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		
		//Ligne 3
		this.att3 = new JLabel("\t"+"\t"+"Nomdre d'�tudiant sans stage pour 2015 : "+"\t"+"\t"+ donnee.getNbEt3());
		this.pnlTab.add(att3);
		
		this.att3.setBackground(new Color(207,227,240));
		this.att3.setOpaque(true);
		this.att3.setFont(fontEntered);
		this.att3.setForeground(new Color(0,51,102));
		this.att3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		//Ligne 4
		this.att4 = new JLabel("\t"+"\t"+"Nombre de stagiaires par entreprise pour n derni�res ann�es : "+"\t"+"\t"+ donnee.getNbEt4());
		this.pnlTab.add(att4);

		this.att4.setBackground(new Color(207,227,240));
		this.att4.setOpaque(true);
		this.att4.setFont(fontEntered);
		this.att4.setForeground(new Color(0,51,102));
		this.att4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		//ligne5
		this.att5 = new JLabel("\t"+"\t"+"Nombre moyen de stagiaires encadr�s par les entreprises pour n derni�res ann�es : "+"\t"+"\t"+ donnee.getMoy());
		this.pnlTab.add(att5);
		
		this.att5.setBackground(new Color(207,227,240));
		this.att5.setOpaque(true);
		this.att5.setFont(fontEntered);
		this.att5.setForeground(new Color(0,51,102));
		this.att5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		//ligne6
		this.att6 = new JLabel("\t"+"\t"+"Nombre de stages par zone g�ographique : "+"\t"+"\t"+ donnee.getNbStageZone() );
		this.pnlTab.add(att6);
		
		this.att6.setBackground(new Color(207,227,240));
		this.att6.setOpaque(true);
		this.att6.setFont(fontEntered);
		this.att6.setForeground(new Color(0,51,102));
		this.att6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		//ligne7
		this.att7 = new JLabel("\t"+"\t"+"Entreprises ayant eu au moins un stage dans les n derni�res ann�es : "+"\t"+"\t"+ donnee.getNbStage() );
		this.pnlTab.add(att7, BorderLayout.WEST);
		
		this.att7.setBackground(new Color(207,227,240));
		this.att7.setOpaque(true);
		this.att7.setFont(fontEntered);
		this.att7.setForeground(new Color(0,51,102));
		this.att7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.add(pnlTab);
		
	}

}
