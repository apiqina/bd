package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import classe.ImagePanel;
import controlleur.EcouteurMenu;

public class VueMenu extends JFrame{

	/**
	 * Attributs
	 */

	//Ecouteur
	EcouteurMenu ecouteurMenu;
	
	//Cr�ation des boutons
	JButton btnNbEtAvecStage;
	JButton btnNbEtSansStage;
	JButton btnNbStagiaireParEntPourNAnnee;
	JButton btnNbStagiaireEncParEntPourNAnnee;
	JButton btnNbStageZone;
	JButton btnListeEntAvecStagePourNAnnee;

	//Image
	private ImagePanel pnlImage;
	private Image image= new ImageIcon("bleu_ciel.png").getImage();
	

	/**
	 * Constructeur
	 */
	public VueMenu(){

		this.ecouteurMenu = new EcouteurMenu(this);
		
		//Propri�t� de la JFrame
		this.setTitle("Menu");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		this.setSize(new Dimension(1200, 950));
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		//Cr�ation des boutons
		this.btnNbEtAvecStage = new JButton("  Nombre d'�tudiants avec stage  ");
		this.btnNbEtSansStage = new JButton("  Nombre d'�tudiants sans stage  ");
		this.btnNbStagiaireParEntPourNAnnee = new JButton("  Nombre de stagiaires par entreprise pour n derni�res ann�es  ");
		this.btnNbStagiaireEncParEntPourNAnnee = new JButton("  Nombre moyen de stagiaires encadr�s par les entreprises pour n derni�res ann�es  ");
		this.btnNbStageZone = new JButton("Nombre de stages par zone g�ographique");
		this.btnListeEntAvecStagePourNAnnee = new JButton("  Entreprises ayant eu au moins un stage dans les n derni�res ann�es  ");

		//Propri�t�s des boutons
		Font fontEntered = new Font(Font.DIALOG, Font.CENTER_BASELINE, 20);
		
		this.btnNbEtAvecStage.setBackground(new Color(207,227,240));
		this.btnNbEtAvecStage.setOpaque(false);
		this.btnNbEtAvecStage.setFont(fontEntered);
		this.btnNbEtAvecStage.setForeground(new Color(0,51,102));
		this.btnNbEtAvecStage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		this.btnNbEtAvecStage.setPreferredSize(new Dimension(900,60));
        this.btnNbEtAvecStage.setVisible(true);
		
		this.btnNbEtSansStage.setBackground(new Color(207,227,240));
		this.btnNbEtSansStage.setOpaque(false);
		this.btnNbEtSansStage.setFont(fontEntered);
		this.btnNbEtSansStage.setForeground(new Color(0,51,102));
		this.btnNbEtSansStage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.btnNbEtSansStage.setPreferredSize(new Dimension(900,60));
        this.btnNbEtSansStage.setVisible(true);
		
		this.btnNbStagiaireParEntPourNAnnee.setBackground(new Color(207,227,240));
		this.btnNbStagiaireParEntPourNAnnee.setOpaque(false);
		this.btnNbStagiaireParEntPourNAnnee.setFont(fontEntered);
		this.btnNbStagiaireParEntPourNAnnee.setForeground(new Color(0,51,102));
		this.btnNbStagiaireParEntPourNAnnee.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.btnNbStagiaireParEntPourNAnnee.setPreferredSize(new Dimension(900,60));
        this.btnNbStagiaireParEntPourNAnnee.setVisible(true);
		
		this.btnNbStagiaireEncParEntPourNAnnee.setBackground(new Color(207,227,240));
		this.btnNbStagiaireEncParEntPourNAnnee.setOpaque(false);
		this.btnNbStagiaireEncParEntPourNAnnee.setFont(fontEntered);
		this.btnNbStagiaireEncParEntPourNAnnee.setForeground(new Color(0,51,102));
		this.btnNbStagiaireEncParEntPourNAnnee.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.btnNbStagiaireEncParEntPourNAnnee.setPreferredSize(new Dimension(900,60));
        this.btnNbStagiaireEncParEntPourNAnnee.setVisible(true);
        
		this.btnNbStageZone.setBackground(new Color(207,227,240));
		this.btnNbStageZone.setOpaque(false);
		this.btnNbStageZone.setFont(fontEntered);
		this.btnNbStageZone.setForeground(new Color(0,51,102));
		this.btnNbStageZone.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.btnNbStageZone.setPreferredSize(new Dimension(900,60));
        this.btnNbStageZone.setVisible(true);
        
		this.btnListeEntAvecStagePourNAnnee.setBackground(new Color(207,227,240));
		this.btnListeEntAvecStagePourNAnnee.setOpaque(false);
		this.btnListeEntAvecStagePourNAnnee.setFont(fontEntered);
		this.btnListeEntAvecStagePourNAnnee.setForeground(new Color(0,51,102));
		this.btnListeEntAvecStagePourNAnnee.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
		this.btnListeEntAvecStagePourNAnnee.setPreferredSize(new Dimension(900,60));
        this.btnListeEntAvecStagePourNAnnee.setVisible(true);
		
		//Panel image qui contient boutons et image
		this.pnlImage = new ImagePanel(image);
		this.pnlImage.setLayout(new BorderLayout());
		
		this.pnlImage.add(btnNbEtAvecStage);
		this.pnlImage.add(btnNbEtSansStage);
		this.pnlImage.add(btnNbStagiaireParEntPourNAnnee);
		this.pnlImage.add(btnNbStagiaireEncParEntPourNAnnee);
		this.pnlImage.add(btnNbStageZone);
		this.pnlImage.add(btnListeEntAvecStagePourNAnnee);

		//Placement de pnlImage
		pnlImage.setLayout(new FlowLayout(FlowLayout.CENTER,10,110));

		//Ajout de pnlImage dans JFrame
		this.add(pnlImage);
		
		//Ajout des listeners
		this.btnNbEtAvecStage.addActionListener(ecouteurMenu);
		this.btnNbEtSansStage.addActionListener(ecouteurMenu);
		this.btnNbStagiaireParEntPourNAnnee.addActionListener(ecouteurMenu);
		this.btnNbStagiaireEncParEntPourNAnnee.addActionListener(ecouteurMenu);
		this.btnNbStageZone.addActionListener(ecouteurMenu);
		this.btnListeEntAvecStagePourNAnnee.addActionListener(ecouteurMenu);

		
	}


}
