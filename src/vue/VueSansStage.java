package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import classe.ImagePanel;
import controlleur.EcouteurSansStage;

public class VueSansStage extends JFrame{

	/**
	 * Attributs
	 */

	private int nbEt;
	
	//Ecouteur
	EcouteurSansStage ecouteurSansStage;

	//Panel
	private ImagePanel pnlImage;
	private Image image= new ImageIcon("bleu_ciel.png").getImage();

	//Label
	JLabel lblNbEtudiantAvecStage;
	
	//Butons
	JButton btnRetour;

	/**
	 * Constructeur
	 */
	public VueSansStage(int nb){

		this.nbEt = nb;
		this.ecouteurSansStage = new EcouteurSansStage(this);

		//Propri�t� de la JFrame
		this.setTitle("Menu");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		this.setSize(new Dimension(650, 400));
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//butons
		this.btnRetour = new JButton("Retour");
		this.btnRetour.setBackground(new Color(210,234,253));
		this.btnRetour.setOpaque(true);
		this.btnRetour.setPreferredSize(new Dimension(100, 60));
		
		//cr�ation label de titre
		Font fontEntered = new Font(Font.DIALOG, Font.CENTER_BASELINE, 20);
		
		this.lblNbEtudiantAvecStage = new JLabel(nbEt + " �tudiants ont un stage");
		this.lblNbEtudiantAvecStage.setPreferredSize(new Dimension(500, 100));
		this.lblNbEtudiantAvecStage.setFont(fontEntered);
		this.lblNbEtudiantAvecStage.setHorizontalAlignment(JLabel.CENTER);
		this.lblNbEtudiantAvecStage.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
		this.lblNbEtudiantAvecStage.setBackground(new Color(210,234,253));
		this.lblNbEtudiantAvecStage.setOpaque(true);
		
		
		//Param�tre + propri�t� de pnlImage
		this.pnlImage = new ImagePanel(image);
		pnlImage.setLayout(new FlowLayout(FlowLayout.CENTER,50,100));
		this.pnlImage.add(lblNbEtudiantAvecStage);
		this.pnlImage.add(btnRetour);
		this.add(pnlImage);
		
		this.btnRetour.addActionListener(ecouteurSansStage);
		
	}

}
